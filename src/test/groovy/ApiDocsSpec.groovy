import api.IsIntegrationSpec
import hacklabor.warehouse.App
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [App])
class ApiDocsSpec extends Specification implements IsIntegrationSpec {

    def 'Can get swagger documentation'() {
        when:
        def result = mockMvc.perform(get('/swagger-ui.html'))

        then:
        result.andExpect(status().isOk())
    }
}
