package api.user

import api.IsIntegrationSpec
import api.TestApi
import hacklabor.warehouse.App
import hacklabor.warehouse.api.user.BorrowItemRequest
import hacklabor.warehouse.api.user.SupplyItemRequest
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

import static api.user.CreateUserRequestBuilder.createUserRequest
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [App])
class UserControllerSpec extends Specification implements IsIntegrationSpec {

    @Subject
    TestApi api

    def setup() {
        api = new TestApi(mockMvc: mockMvc)
    }

    def 'Can create a user'() {
        given: 'A request to create a user'
        def request = createUserRequest().id(null).build()

        when: 'performing a request to create a user'
        def result = api.createUser(request)

        then: 'the user is created'
        result.andExpect(status().isCreated())
    }

    def 'Can get a user by id'() {
        given: 'A user exists'
        def user = userExists()

        when: 'performing a get to fetch the user'
        def result = api.getUserById(user)

        then: 'the user is returned'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').value(user.id))
        result.andExpect(jsonPath('$.data.userName').value(user.userName))
    }

    def 'Can get a user by rfid chip id'() {
        given: 'A user request with a specific rfid'
        def request = createUserRequest().rfidChipId('someSpecificRfid').build()

        when: 'the user is created'
        api.createUser(request)

        and: 'getting the user by the specific rfid'
        def result = api.getUserByRfid('someSpecificRfid')

        then: 'the user is returned'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').isNotEmpty())
        result.andExpect(jsonPath('$.data.rfidChipId').value('someSpecificRfid'))
    }

    def 'User can supply an item which updates the storage unit'() {
        given: 'A user can supply an item to a storage unit'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit.itemCount[item.id] = 23L
        storageUnit = storageUnitRepository.save(storageUnit)

        def request = new SupplyItemRequest()
        request.storageUnitId = storageUnit.id
        request.itemId = item.id
        request.itemCount = 23

        when: 'sending a POST request to supply the item'
        def result = api.supplyItem(user, request)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the storage unit contains the specified amount of that item'
        def storageUnitResponse = api.getStorageUnit(storageUnit.id)
        storageUnitResponse.andExpect(jsonPath('$.data.items[0].id').value(item.id))
        storageUnitResponse.andExpect(jsonPath("\$.data.items[0].availableCount").value(46))
    }


    def 'User can supply an item which updates the users inventory'() {
        given: 'A user can supply an item to a storage unit'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit = storageUnitRepository.save(storageUnit)

        def request = new SupplyItemRequest()
        request.storageUnitId = storageUnit.id
        request.itemId = item.id
        request.itemCount = 23

        when: 'sending a POST request to supply the item'
        def result = api.supplyItem(user, request)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the users inventory contains the specified amount of that item'
        def userResponse = api.getUserById(user)
        userResponse.andExpect(jsonPath("\$.data.inventory.${item.id}.count").value(23))
    }

    def 'A user can supply one of an item twice in a row'() {
        given: 'A user can supply an item to a storage unit'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit = storageUnitRepository.save(storageUnit)

        def request = new SupplyItemRequest()
        request.storageUnitId = storageUnit.id
        request.itemId = item.id
        request.itemCount = 1

        when: 'sending two POST requests to supply one of the item'
        api.supplyItem(user, request)
        def result = api.supplyItem(user, request)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the users inventory contains two of the item'
        def userResponse = api.getUserById(user)
        userResponse.andExpect(jsonPath("\$.data.inventory.${item.id}.count").value(2))
    }

    def 'User can borrow an item which updates the users inventory'() {
        given: 'A user can borrow an item from a storage unit'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit = storageUnitRepository.save(storageUnit)

        def request = new BorrowItemRequest()
        request.storageUnitId = storageUnit.id
        request.itemId = item.id
        request.itemCount = 1

        when: 'sending a POST request to borrow the item'
        def result = api.borrowItem(user, request)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the storage unit contains the specified amount of that item'
        def userResponse = api.getUserById(user)
        userResponse.andExpect(jsonPath("\$.data.inventory.${item.id}.count").value(-1))
    }

    def 'User can borrow an item which updates the storage unit'() {
        given: 'A user can borrow an item from a storage unit'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit.itemCount[item.id] = 23L
        storageUnit = storageUnitRepository.save(storageUnit)

        def request = new BorrowItemRequest()
        request.storageUnitId = storageUnit.id
        request.itemId = item.id
        request.itemCount = 5

        when: 'sending a POST request to borrow the item'
        def result = api.borrowItem(user, request)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the storage unit contains the specified amount of that item'
        def storageUnitResponse = api.getStorageUnit(storageUnit.id)
        storageUnitResponse.andExpect(jsonPath('$.data.items[0].id').value(item.id))
        storageUnitResponse.andExpect(jsonPath("\$.data.items[0].availableCount").value(18))
    }

    def 'Can get the inventory history of a user'() {
        given: 'A a user has borrowed an item'
        def user = userExists()
        def item = itemExists()
        def storageUnit = storageUnitExists()
        storageUnit.addItem(item)
        storageUnit.itemCount[item.id] = 23L
        storageUnit = storageUnitRepository.save(storageUnit)

        def borrowRequest = new BorrowItemRequest()
        borrowRequest.storageUnitId = storageUnit.id
        borrowRequest.itemId = item.id
        borrowRequest.itemCount = 5
        api.borrowItem(user, borrowRequest)

        when: 'sending a GET request to get the users inventory history'
        def result = api.getUserInventoryHistory(user)

        then: 'the users inventory history is returned'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data').isArray())
        result.andExpect(jsonPath('$.data[0].type').value('BORROW'))
        result.andExpect(jsonPath('$.data[0].itemId').value(borrowRequest.itemId))
        result.andExpect(jsonPath('$.data[0].itemCount').value(borrowRequest.itemCount))
    }
}
