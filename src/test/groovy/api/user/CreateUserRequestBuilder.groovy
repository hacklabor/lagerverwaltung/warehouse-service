package api.user

import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import hacklabor.warehouse.api.user.CreateUserRequest
import org.apache.commons.lang3.RandomStringUtils

import java.util.concurrent.ThreadLocalRandom

@Builder(builderStrategy = ExternalStrategy, forClass = CreateUserRequest)
class CreateUserRequestBuilder {

    public static CreateUserRequestBuilder createUserRequest() {
        return new CreateUserRequestBuilder()
                .id(ThreadLocalRandom.current().nextInt(0, 99))
                .userName(RandomStringUtils.randomAlphanumeric(12))
                .rfidChipId(RandomStringUtils.randomAlphanumeric(12))
    }
}
