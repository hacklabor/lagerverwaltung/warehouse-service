package api.items

import api.IsIntegrationSpec
import api.TestApi
import groovy.json.JsonOutput
import hacklabor.warehouse.App
import hacklabor.warehouse.api.items.CreateItemRequest
import hacklabor.warehouse.api.storage.unit.AddItemToStorageUnitRequest
import hacklabor.warehouse.domain.items.ItemTypes
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import spock.lang.Specification
import spock.lang.Subject

import static api.items.CreateItemRequestBuilder.createItemRequest
import static api.items.UpdateItemRequestBuilder.updateItemRequest
import static org.hamcrest.Matchers.hasSize
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [App])
class ItemControllerSpec extends Specification implements IsIntegrationSpec {

    @Subject
    TestApi api

    def setup() {
        api = new TestApi(mockMvc: mockMvc)
    }

    def 'Can create item'() {
        given:
        def imageUrl = 'http://example.com/images/42'

        CreateItemRequest request = createItemRequest()
                .imageUrl(imageUrl)
                .build()

        when:
        def result = api.createItem(request)

        then:
        result.andExpect(status().isCreated())
        result.andExpect(jsonPath('$.data.imageUrl').value(imageUrl))
    }

    def 'Can create an item with a specific id'() {
        given: 'A request to create an item with a specific id'
        def request = createItemRequest()
                .id(42)
                .build()

        when: 'performing a POST request'
        def result = api.createItem(request)

        then: 'the item is 201 created'
        result.andExpect(status().isCreated())

        when: 'performing a GET to fetch the persisted item'
        result = mockMvc.perform(get("/items/42"))

        then: 'result is 200 OK and has the specific id.'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').value(42))
    }

    def 'Can get item'() {
        given:
        def item = itemExists()

        when:
        def result = mockMvc.perform(get("/items/${item.id}"))

        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').value(item.id))
        result.andExpect(jsonPath('$.data.name').value(item.name))
    }

    def 'Can get all items'() {
        given:
        itemExists()
        itemExists()

        when:
        def result = mockMvc.perform(get('/items'))

        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data').isArray())
        result.andExpect(jsonPath('$.data').isNotEmpty())
    }

    def 'Creating an item with an invalid replacement url returns a HTTP 400'() {
        given:
        def request = new CreateItemRequest(ItemTypes.REPLACEABLE)
        request.replacementUrl = 'not an url'

        when:
        def result = mockMvc.perform(post('/items')
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))

        then:
        result.andExpect(status().isBadRequest())
        result.andExpect(jsonPath('$.errorMessage').isNotEmpty())
    }

    def 'Can edit an item'() {
        given:
        def item = itemExists()
        def newItemName = 'New item name'
        def newImageUrl = 'http://example.com/new/url'
        def request = updateItemRequest()
                .name(newItemName)
                .imageUrl(newImageUrl)
                .build()

        when:
        def result = mockMvc.perform(put("/items/${item.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))

        then:
        result.andExpect(status().isOk())

        when:
        result = mockMvc.perform(get("/items/${item.id}"))

        then:
        result.andExpect(jsonPath('$.data.name').value(newItemName))
        result.andExpect(jsonPath('$.data.imageUrl').value(newImageUrl))
    }

    def 'Can not update an item with invalid data'() {
        given:
        def item = itemExists()
        def request = updateItemRequest()
                .replacementUrl("not an url")
                .build()

        when:
        def result = mockMvc.perform(put("/items/${item.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))

        then:
        result.andExpect(status().isBadRequest())
        result.andExpect(jsonPath('$.errorMessage').isNotEmpty())
    }

    def 'Can add an item to a storage unit'() {
        given: 'An item and a storage unit exist'
        def item = itemExists()
        def storageUnit = storageUnitExists()
        def request = new AddItemToStorageUnitRequest(item.id)

        when: 'adding the item'
        api.addItemToUnit(storageUnit, request)

        and: 'fetching it again'
        def result = api.getItemById(item.id)

        then: 'it contains the assigned storage unit'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.storageUnits').isArray())
        result.andExpect(jsonPath('$.data.storageUnits[0].id').value(storageUnit.id))
    }

    def 'Can query items by name'() {
        given:
        def itemNameToSearchFor = RandomStringUtils.randomAlphanumeric(12)
        itemExists("abc${itemNameToSearchFor}def")
        itemExists("${itemNameToSearchFor}123")
        itemExists("sdaf11${itemNameToSearchFor}")
        itemExists('ESP8266')
        itemExists('RGB LED WS2812')

        when:
        def result = api.queryItemByName(itemNameToSearchFor)

        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data', hasSize(3)))
    }

    def 'Can delete an item'() {
        given: 'An item exists'
        def item = itemExists()

        and: 'the item can be queried'
        api.getItems().andExpect(jsonPath("\$.data[?(@.id == ${item.id})]").exists())


        when: 'sending a DELETE request for the item'
        def result = api.deleteItem(item)

        then: 'the result is 204 no content'
        result.andExpect(status().isNoContent())

        and: 'the item will be marked as deleted'
        api.getItems().andExpect(jsonPath("\$.data[?(@.id == ${item.id})].deleted").value(true))

        when: 'when querying for not deleted items'
        result = api.getItems('', false)

        then: 'the item is not returned'
        result.andExpect(jsonPath("\$.data[?(@.id == ${item.id})]").doesNotExist())
    }
}
