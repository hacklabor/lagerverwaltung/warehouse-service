package api.items

import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import hacklabor.warehouse.api.items.CreateItemRequest
import hacklabor.warehouse.domain.items.ItemTypes

@Builder(builderStrategy = ExternalStrategy, forClass = CreateItemRequest)
public class CreateItemRequestBuilder {

    public static CreateItemRequestBuilder createItemRequest() {
        new CreateItemRequestBuilder()
                .itemType(ItemTypes.REPLACEABLE)
                .name('Raspberry Pi Zero W')
                .description('Lorem ipsum dolor.')
                .replacementUrl('https://example.com/pi/zero/w')
    }
}
