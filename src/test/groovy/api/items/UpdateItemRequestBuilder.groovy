package api.items


import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import hacklabor.warehouse.api.items.UpdateItemRequest

@Builder(builderStrategy = ExternalStrategy, forClass = UpdateItemRequest)
public class UpdateItemRequestBuilder {
    public static UpdateItemRequestBuilder updateItemRequest() {
        new UpdateItemRequestBuilder()
    }
}
