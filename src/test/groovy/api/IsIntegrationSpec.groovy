package api

import hacklabor.warehouse.domain.items.Item
import hacklabor.warehouse.domain.items.ItemRepository
import hacklabor.warehouse.domain.items.ReplaceableItem
import hacklabor.warehouse.domain.storage.StorageUnit
import hacklabor.warehouse.domain.storage.StorageUnitRepository
import hacklabor.warehouse.domain.user.User
import hacklabor.warehouse.domain.user.UserRepository
import org.apache.commons.lang3.RandomStringUtils
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Shared

trait IsIntegrationSpec {

    @Shared
    @Autowired
    MockMvc mockMvc

    @Autowired
    StorageUnitRepository storageUnitRepository

    @Autowired
    ItemRepository itemRepository

    @Autowired
    UserRepository userRepository


    StorageUnit storageUnitExists() {
        def storageUnit = new StorageUnit()
        storageUnitRepository.save(storageUnit)
    }

    Item itemExists(String name = 'my item') {
        def item = new ReplaceableItem()
        item.name = name
        item.description = 'This is my item.'
        itemRepository.save(item)
    }

    User userExists() {
        def user = new User()
        user.userName = RandomStringUtils.randomAlphanumeric(12)
        user.rfidChipId = RandomStringUtils.randomAlphanumeric(12)
        userRepository.save(user)
    }
}
