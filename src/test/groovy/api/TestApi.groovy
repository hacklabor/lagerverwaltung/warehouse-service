package api

import groovy.json.JsonOutput
import hacklabor.warehouse.api.items.CreateItemRequest
import hacklabor.warehouse.api.items.UpdateItemCountRequest
import hacklabor.warehouse.api.storage.unit.AddItemToStorageUnitRequest
import hacklabor.warehouse.api.storage.unit.CreateStorageUnitRequest
import hacklabor.warehouse.api.storage.unit.UpdateStorageUnitRequest
import hacklabor.warehouse.api.user.BorrowItemRequest
import hacklabor.warehouse.api.user.CreateUserRequest
import hacklabor.warehouse.api.user.SupplyItemRequest
import hacklabor.warehouse.domain.items.Item
import hacklabor.warehouse.domain.storage.StorageUnit
import hacklabor.warehouse.domain.user.User
import lombok.AllArgsConstructor
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@AllArgsConstructor
class TestApi {

    MockMvc mockMvc

    def createStorageUnit(CreateStorageUnitRequest request) {
        mockMvc.perform(post("/storageUnits")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def createItem(CreateItemRequest request) {
        mockMvc.perform(post("/items")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def addItemToUnit(StorageUnit storageUnit, AddItemToStorageUnitRequest request) {
        mockMvc.perform(post("/storageUnits/${storageUnit.id}/items")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def getStorageUnit(Long storageUnitId) {
        mockMvc.perform(get("/storageUnits/${storageUnitId}"))
    }

    def getAllStorageUnits() {
        mockMvc.perform(get("/storageUnits"))
    }

    def updateStorageUnit(Long storageUnitId, UpdateStorageUnitRequest request) {
        mockMvc.perform((put("/storageUnits/${storageUnitId}"))
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def updateItemCount(StorageUnit storageUnit, Item item, UpdateItemCountRequest request) {
        mockMvc.perform(post("/storageUnits/${storageUnit.id}/items/${item.id}")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def getItemById(long itemId) {
        mockMvc.perform(get("/items/${itemId}"))
    }

    def queryItemByName(String name) {
        mockMvc.perform(get("/items?name=${name}"))
    }

    def createUser(CreateUserRequest request) {
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def getUserById(User user) {
        mockMvc.perform(get("/users/${user.id}"))
    }

    def getUserByRfid(String rfid) {
        mockMvc.perform(get("/users/rfidChipId/${rfid}"))
    }

    def supplyItem(User user, SupplyItemRequest request){
        mockMvc.perform(post("/users/${user.id}/supply")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def borrowItem(User user, BorrowItemRequest request) {
        mockMvc.perform(post("/users/${user.id}/borrow")
                .contentType(MediaType.APPLICATION_JSON)
                .content(JsonOutput.toJson(request)))
    }

    def getUserInventoryHistory(User user) {
        mockMvc.perform(get("/users/${user.id}/inventory/history"))
    }

    def getItems(String name = '', boolean includeDeleted = true){
        mockMvc.perform(get("/items?name=${name}&includeDeleted=${includeDeleted}"))
    }

    def deleteItem(Item item) {
        mockMvc.perform(delete("/items/${item.id}"))
    }
}
