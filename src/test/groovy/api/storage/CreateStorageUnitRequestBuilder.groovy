package api.storage

import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import hacklabor.warehouse.api.storage.unit.CreateStorageUnitRequest

@Builder(builderStrategy = ExternalStrategy, forClass = CreateStorageUnitRequest)
public class CreateStorageUnitRequestBuilder {
    public static CreateStorageUnitRequestBuilder createStorageUnitRequest() {
        new CreateStorageUnitRequestBuilder()
                .shelfId(1)
                .row(23)
                .column(42)
    }
}
