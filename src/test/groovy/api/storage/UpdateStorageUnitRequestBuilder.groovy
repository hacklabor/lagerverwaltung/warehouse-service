package api.storage

import groovy.transform.builder.Builder
import groovy.transform.builder.ExternalStrategy
import hacklabor.warehouse.api.storage.unit.UpdateStorageUnitRequest

@Builder(builderStrategy = ExternalStrategy, forClass = UpdateStorageUnitRequest)
class UpdateStorageUnitRequestBuilder {

    public static UpdateStorageUnitRequestBuilder updateStorageUnitRequest() {
        new UpdateStorageUnitRequestBuilder()
    }
}
