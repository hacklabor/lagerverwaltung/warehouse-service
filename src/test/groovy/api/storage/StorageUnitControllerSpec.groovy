package api.storage

import api.IsIntegrationSpec
import api.TestApi
import hacklabor.warehouse.App
import hacklabor.warehouse.api.items.UpdateItemCountRequest
import hacklabor.warehouse.api.storage.unit.AddItemToStorageUnitRequest
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification
import spock.lang.Subject

import static api.storage.CreateStorageUnitRequestBuilder.createStorageUnitRequest
import static api.storage.UpdateStorageUnitRequestBuilder.updateStorageUnitRequest
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [App])
class StorageUnitControllerSpec extends Specification implements IsIntegrationSpec {

    @Subject
    TestApi api

    def setup() {
        api = new TestApi(mockMvc: mockMvc)
    }

    def 'Can create a storage unit with data about it\'s location'() {
        given:
        def request = createStorageUnitRequest().build()

        when:
        def result = api.createStorageUnit(request)

        then:
        result.andExpect(status().isCreated())
        result.andExpect(jsonPath('$.data.id').isNumber())
        result.andExpect(jsonPath('$.data.shelfId').value(1))
        result.andExpect(jsonPath('$.data.row').value(23))
        result.andExpect(jsonPath('$.data.column').value(42))
    }

    def 'Can create a storage unit with a specific id'() {
        given: 'A request to create a storage unit with a specific id'
        def request = createStorageUnitRequest().id(42).build()

        when: 'performing a POST request'
        def result = api.createStorageUnit(request)

        then: 'the storage unit is 201 created'
        result.andExpect(status().isCreated())

        when: 'performing a GET to fetch the persisted storage unit'
        result = api.getStorageUnit(42)

        then: 'result is 200 OK and has the specific id.'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').value(42))
    }

    def 'Can not create two storage units with the same id'() {
        given: 'A request to create a storage unit with a specific id'
        def request = createStorageUnitRequest().id(999).build()

        when: 'performing a POST request'
        def result = api.createStorageUnit(request)

        then: 'the storage unit is 201 created'
        result.andExpect(status().isCreated())

        when: 'performing the request with the same id again'
        result = api.createStorageUnit(request)

        then: 'result is 409 CONFLICT'
        result.andExpect(status().isConflict())
    }

    def 'Can get all storage units'() {
        given:
        storageUnitExists()

        when:
        def result = api.getAllStorageUnits()

        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data').isArray())
        result.andExpect(jsonPath('$.data').isNotEmpty())
    }

    def 'Can get storage unit by id'() {
        given:
        def storageUnit = storageUnitExists()

        when:
        def result = api.getStorageUnit(storageUnit.id)

        then:
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.id').value(storageUnit.id))
    }

    def 'Can update a storage unit'() {
        given: 'A storage unit can be updated'
        def newItem = itemExists()
        def anotherNewItem = itemExists()
        def storageUnit = storageUnitExists()
        def request = updateStorageUnitRequest()
                .shelfId(1337)
                .row(12)
                .column(34)
                .itemIds([newItem.id, anotherNewItem.id])
                .build()

        when: 'sending a PUT request to update the storage unit'
        def result = api.updateStorageUnit(storageUnit.id, request)

        then: 'the response is 200 OK'
        result.andExpect(status().isOk())

        when: 'fetching the storage unit now'
        result = api.getStorageUnit(storageUnit.id)

        then: 'storage unit has the new properties from the request'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.shelfId').value(1337))
        result.andExpect(jsonPath('$.data.row').value(12))

        and: 'contains both items'
        result.andExpect(jsonPath("\$.data.items[?(@.id == ${newItem.id})]").exists())
        result.andExpect(jsonPath("\$.data.items[?(@.id == ${anotherNewItem.id})]").exists())
    }

    def 'Can add an item to a storage unit'() {
        given: 'A storage unit and an item exist'
        def storageUnit = storageUnitExists()
        def item = itemExists()
        def request = new AddItemToStorageUnitRequest(item.id)

        when: 'sending a POST request to add the item to the unit'
        def result = api.addItemToUnit(storageUnit, request)

        then: 'the result is 201 Created'
        result.andExpect(status().isCreated())
        result.andExpect(jsonPath('$.data').isNotEmpty())

        when: 'getting the unit now'
        result = api.getStorageUnit(storageUnit.id)

        then: 'the unit contains the item'
        result.andExpect(status().isOk())
        result.andExpect(jsonPath('$.data.items').isArray())
        result.andExpect(jsonPath('$.data.items').isNotEmpty())
        result.andExpect(jsonPath('$.data.items[0].id').value(item.id))
    }

    def 'Can set the item count for an item that is stored in a unit'() {
        given: 'An item is stored in a unit'
        def storageUnit = storageUnitExists()
        def item = itemExists()
        api.addItemToUnit(storageUnit, new AddItemToStorageUnitRequest(item.id))

        def request = new UpdateItemCountRequest(10L)

        when: 'performing a POST request to update the count of the item'
        def result = api.updateItemCount(storageUnit, item, request)

        then: 'the result is OK'
        result.andExpect(status().isOk())

        and: 'when getting the unit, the count of the item is updated'
        def updatedStorageUnit = api.getStorageUnit(storageUnit.id)
        updatedStorageUnit.andExpect(jsonPath('$.data.items[0].availableCount').value(10))
    }
}
