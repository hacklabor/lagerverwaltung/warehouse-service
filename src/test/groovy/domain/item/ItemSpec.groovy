package domain.item

import hacklabor.warehouse.App
import hacklabor.warehouse.domain.items.ItemRepository
import hacklabor.warehouse.domain.items.ReplaceableItem
import hacklabor.warehouse.domain.storage.StorageUnit
import hacklabor.warehouse.domain.storage.StorageUnitRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.context.SpringBootTest
import spock.lang.Specification

@AutoConfigureTestDatabase
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = [App])
class ItemSpec extends Specification {

    @Autowired
    ItemRepository itemRepository

    @Autowired
    StorageUnitRepository storageUnitRepository

    def 'Can add an item to multiple storage units'() {
        given: 'An item and two storage units exist'
        def item = new ReplaceableItem()
        item.name = "someItem"
        item = itemRepository.save(item)
        def storageUnit = storageUnitRepository.save(new StorageUnit())
        def otherStorageUnit = storageUnitRepository.save(new StorageUnit())

        when: 'adding the item to the first unit'
        storageUnit.addItem(item)
        storageUnitRepository.save(storageUnit)

        and: 'adding the item to the second unit'
        otherStorageUnit.addItem(item)
        storageUnitRepository.save(otherStorageUnit)

        then: 'the item is both units'
        item.storageUnits.size() == 2
        item.storageUnits.contains(storageUnit)
        item.storageUnits.contains(otherStorageUnit)
    }
}
