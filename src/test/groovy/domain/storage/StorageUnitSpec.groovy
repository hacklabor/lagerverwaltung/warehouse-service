package domain.storage

import hacklabor.warehouse.domain.items.ReplaceableItem
import hacklabor.warehouse.domain.storage.StorageUnit
import spock.lang.Specification

class StorageUnitSpec extends Specification {

    def 'Items are exposed as a readonly collection'() {
        given:
        def storageUnit = new StorageUnit()

        when:
        storageUnit.getItems().add(new ReplaceableItem())

        then:
        thrown(UnsupportedOperationException)
    }

    def 'Items can be added'() {
        given: 'A storage unit exists'
        def storageUnit = new StorageUnit()
        def item = new ReplaceableItem()

        when: 'adding an item'
        storageUnit.addItem(item)

        then: 'the unit contains the item'
        storageUnit.items.size() == 1
        storageUnit.items.contains(item)
    }

    def 'Adding an item twice will only add the item once'() {
        given: 'A storage unit exists'
        def storageUnit = new StorageUnit()
        def item = new ReplaceableItem()

        when: 'adding an item twice'
        storageUnit.addItem(item)
        storageUnit.addItem(item)

        then: 'the unit will contain the item only once'
        storageUnit.items.size() == 1
    }

    def 'Can increase the item count of a given item'(){
        given: 'A storage unit exists'
        def storageUnit = new StorageUnit()
        def item = new ReplaceableItem()
        item.id = 42
        storageUnit.itemCount[item.id] = 23L

        when:
        storageUnit.increaseItemCount(item, 23)

        then:
        storageUnit.itemCount[item.id] == 46L
    }

    def 'Can decrease the item count of a given item'(){
        given: 'A storage unit exists'
        def storageUnit = new StorageUnit()
        def item = new ReplaceableItem()
        item.id = 42
        storageUnit.itemCount[item.id] = 23L

        when:
        storageUnit.decreaseItemCount(item, 23)

        then:
        storageUnit.itemCount[item.id] == 0L
    }
}
