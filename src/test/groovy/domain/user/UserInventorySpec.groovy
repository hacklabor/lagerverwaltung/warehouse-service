package domain.user

import hacklabor.warehouse.domain.items.ReplaceableItem
import hacklabor.warehouse.domain.user.SupplyItem
import hacklabor.warehouse.domain.user.BorrowItem
import hacklabor.warehouse.domain.user.User
import spock.lang.Specification

class UserInventorySpec extends Specification {

    def 'Can get an empty user inventory'() {
        given:
        def user = new User()

        when:
        def inventory = user.getInventory()

        then:
        inventory != null
        inventory.itemCount != null
        inventory.items.isEmpty()
    }

    def 'User can supply an new item to his inventory'() {
        given:
        def user = new User()
        def item = new ReplaceableItem()

        when:
        new SupplyItem(user, item, 23)
        def inventory = user.getInventory()

        then:
        inventory.items.size() == 1
        inventory.items.contains(item)
        inventory.getItemCount(item) == 23
    }

    def 'Can increase item count of an existing item by supplying more items'() {
        given:
        def user = new User()
        def item = new ReplaceableItem()

        when:
        new SupplyItem(user, item, 23)
        new SupplyItem(user, item, 19)
        def inventory = user.getInventory()

        then:
        inventory.items.size() == 1
        inventory.items.contains(item)
        inventory.getItemCount(item) == 42
    }

    def 'Can borrow an item that is already in the inventory'() {
        given:
        def user = new User()
        def item = new ReplaceableItem()

        when:
        new SupplyItem(user, item, 42)
        new BorrowItem(user, item, 1)
        def inventory = user.getInventory()

        then:
        inventory.items.size() == 1
        inventory.items.contains(item)
        inventory.getItemCount(item) == 41
    }

    def 'Can borrow an item that is not already the inventory'() {
        given:
        def user = new User()
        def item = new ReplaceableItem()

        when:
        new BorrowItem(user, item, 1)
        def inventory = user.getInventory()

        then:
        inventory.items.size() == 1
        inventory.items.contains(item)
        inventory.getItemCount(item) == -1
    }
}
