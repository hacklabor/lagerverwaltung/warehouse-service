CREATE TABLE IF NOT EXISTS user_inventory_events
(
    id           bigserial NOT NULL,
    created_at   timestamp NULL,
    effective_at timestamp NULL,
    user_id      int8      NOT NULL,
    CONSTRAINT user_inventory_event_pkey PRIMARY KEY (id),
    CONSTRAINT fk_user_inventory_event_user FOREIGN KEY (user_id) REFERENCES users (id)
);

CREATE TABLE IF NOT EXISTS supply_items_events
(
    item_count int8 NULL,
    id         int8 NOT NULL,
    item_id    int8 NOT NULL,
    CONSTRAINT supply_items_events_pkey PRIMARY KEY (id),
    CONSTRAINT fk_supply_items_events_user_inventory_event FOREIGN KEY (id) REFERENCES user_inventory_events (id),
    CONSTRAINT fk_supply_items_events_item FOREIGN KEY (item_id) REFERENCES item (id)
);

CREATE TABLE IF NOT EXISTS borrow_item_events
(
    item_count int8 NULL,
    id         int8 NOT NULL,
    item_id    int8 NOT NULL,
    CONSTRAINT borrow_item_events_pkey PRIMARY KEY (id),
    CONSTRAINT fk_borrow_item_events_user_inventory_event FOREIGN KEY (id) REFERENCES user_inventory_events (id),
    CONSTRAINT fk_borrow_item_events_item FOREIGN KEY (item_id) REFERENCES item (id)
);