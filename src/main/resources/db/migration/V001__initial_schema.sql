CREATE TABLE IF NOT EXISTS storage_units
(
    id             bigserial NOT NULL,
    storage_column int8      NULL,
    storage_row    int8      NULL,
    shelf_id       int8      NULL,
    CONSTRAINT storage_units_pkey PRIMARY KEY (id)
);

CREATE TABLE IF NOT EXISTS item
(
    id              bigserial     NOT NULL,
    description     varchar(2048) NULL,
    image_url       varchar(255)  NULL,
    item_name       varchar(1024) NULL,
    storage_unit_id int8          NULL,
    CONSTRAINT item_pkey PRIMARY KEY (id),
    CONSTRAINT fk_item_storage_unit FOREIGN KEY (storage_unit_id) REFERENCES storage_units (id)
);


CREATE TABLE IF NOT EXISTS replaceable_item
(
    number_of_items_leased int4         NOT NULL,
    replacement_url        varchar(255) NULL,
    id                     int8         NOT NULL,
    CONSTRAINT replaceable_item_pkey PRIMARY KEY (id),
    CONSTRAINT fk_inheritance_replaceable_item_item FOREIGN KEY (id) REFERENCES item (id)
);

CREATE TABLE IF NOT EXISTS storage_unit_item_count
(
    storage_unit_id int8 NOT NULL,
    item_count      int8 NULL,
    id              int8 NOT NULL,
    CONSTRAINT storage_unit_item_count_pkey PRIMARY KEY (storage_unit_id, id),
    CONSTRAINT fk_storage_unit_item_count_storage_unit FOREIGN KEY (storage_unit_id) REFERENCES storage_units (id)
);

CREATE TABLE IF NOT EXISTS storage_unit_items
(
    item_id         int8 NOT NULL,
    storage_unit_id int8 NOT NULL,
    CONSTRAINT storage_unit_items_pkey PRIMARY KEY (item_id, storage_unit_id),
    CONSTRAINT fk_storage_unit FOREIGN KEY (storage_unit_id) REFERENCES item (id),
    CONSTRAINT fk_item FOREIGN KEY (item_id) REFERENCES storage_units (id)
);
