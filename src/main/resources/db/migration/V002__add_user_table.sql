CREATE TABLE IF NOT EXISTS users
(
    id           bigserial    NOT NULL,
    rfid_chip_id varchar(255) NULL,
    user_name    varchar(255) NULL,
    CONSTRAINT uk_rfid_chip_id UNIQUE (rfid_chip_id),
    CONSTRAINT uk_user_name UNIQUE (user_name),
    CONSTRAINT users_pkey PRIMARY KEY (id)
);
