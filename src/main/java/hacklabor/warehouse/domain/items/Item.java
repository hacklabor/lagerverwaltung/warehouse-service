package hacklabor.warehouse.domain.items;

import hacklabor.warehouse.api.items.CreateItemRequest;
import hacklabor.warehouse.api.items.UpdateItemRequest;
import hacklabor.warehouse.domain.storage.StorageUnit;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@NoArgsConstructor
public abstract class Item {

    @Id
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "hacklabor.warehouse.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    @Column(unique = true, nullable = false)
    protected Long id;

    @NotBlank
    @Size(min = 5, max = 1024)
    @Column(name = "item_name")
    protected String name;

    @Size(max = 2048)
    protected String description;

    @URL
    protected String imageUrl;

    protected boolean isDeleted;

    @ManyToMany(mappedBy = "items")
    @Setter(AccessLevel.PRIVATE)
    @EqualsAndHashCode.Exclude
    protected Set<StorageUnit> storageUnits = new HashSet<>();

    public static Item create(@Valid CreateItemRequest request) {
        Item newItem = Item.from(request);

        newItem.setId(request.getId());
        newItem.setName(request.getName());
        newItem.setDescription(request.getDescription());
        newItem.setImageUrl(request.getImageUrl());

        return newItem;
    }

    private static Item from(@Valid CreateItemRequest request) {
        switch (request.getItemType()) {
            case REPLACEABLE:
                ReplaceableItem newItem = new ReplaceableItem();

                newItem.setReplacementUrl(request.getReplacementUrl());
                newItem.setNumberOfItemsLeased(0);

                return newItem;
            default:
                throw new IllegalArgumentException("Can not creat an item for item type " + request.getItemType());
        }
    }

    public void addToStorageUnit(StorageUnit storageUnit) {
        this.storageUnits.add(storageUnit);
    }

    @Valid
    public abstract Item update(UpdateItemRequest request);
}
