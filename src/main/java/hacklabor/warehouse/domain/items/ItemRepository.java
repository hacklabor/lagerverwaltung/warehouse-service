package hacklabor.warehouse.domain.items;

import org.springframework.data.repository.CrudRepository;

public interface ItemRepository extends CrudRepository<Item, Long> {
    Iterable<Item> findByNameContainingIgnoreCase(String name);
}
