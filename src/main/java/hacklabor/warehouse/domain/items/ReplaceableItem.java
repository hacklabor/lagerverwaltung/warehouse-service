package hacklabor.warehouse.domain.items;

import hacklabor.warehouse.api.items.UpdateItemRequest;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Entity;
import javax.validation.constraints.PositiveOrZero;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
public class ReplaceableItem extends Item {

    @URL
    private String replacementUrl;

    @PositiveOrZero
    private int numberOfItemsLeased;

    @Override
    public Item update(UpdateItemRequest request) {
        this.name = request.getName();
        this.description = request.getDescription();
        this.replacementUrl = request.getReplacementUrl();
        this.imageUrl = request.getImageUrl();

        return this;
    }
}
