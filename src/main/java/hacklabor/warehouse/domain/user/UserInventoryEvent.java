package hacklabor.warehouse.domain.user;

import hacklabor.warehouse.api.user.UserInventoryEventResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Data
@Entity
@NoArgsConstructor
@Table(name = "user_inventory_events")
@Inheritance(strategy = InheritanceType.JOINED)
@EqualsAndHashCode(of = "id")
public abstract class UserInventoryEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(unique = true, nullable = false)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    private ZonedDateTime createdAt;

    private ZonedDateTime effectiveAt;

    protected UserInventoryEvent(User user) {
        this.createdAt = ZonedDateTime.now();
        this.effectiveAt = ZonedDateTime.now();

        this.user = user;
        user.addUserInventoryEvent(this);
    }

    public abstract Inventory applyTo(Inventory inventory);

    public abstract UserInventoryEventResponse toResponse();
}
