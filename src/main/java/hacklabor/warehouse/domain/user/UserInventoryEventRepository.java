package hacklabor.warehouse.domain.user;

import org.springframework.data.repository.CrudRepository;

public interface UserInventoryEventRepository extends CrudRepository<UserInventoryEvent, Long> {
}
