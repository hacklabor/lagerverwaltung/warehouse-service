package hacklabor.warehouse.domain.user;

import hacklabor.warehouse.api.user.CreateUserRequest;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@Table(name = "users")
public class User {

    @Id
    @Column(unique = true, nullable = false)
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "hacklabor.warehouse.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    private Long id;

    @NotBlank
    @Column(unique = true, name = "user_name")
    private String userName;

    @Column(unique = true, name = "rfid_chip_id")
    private String rfidChipId;

    @Setter(AccessLevel.PRIVATE)
    @OneToMany(mappedBy = "user")
    private Set<UserInventoryEvent> inventoryEvents = new HashSet<>();

    public static User from(CreateUserRequest request) {
        User user = new User();

        user.setId(request.getId());
        user.setUserName(request.getUserName());
        user.setRfidChipId(request.getRfidChipId());

        return user;
    }

    public Set<UserInventoryEvent> getInventoryEvents(){
        return Collections.unmodifiableSet(inventoryEvents);
    }

    public Inventory getInventory() {
        return Inventory.forUser(this);
    }

    public void addUserInventoryEvent(UserInventoryEvent event) {
        inventoryEvents.add(event);
    }
}
