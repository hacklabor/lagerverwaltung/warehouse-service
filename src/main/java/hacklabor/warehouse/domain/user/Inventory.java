package hacklabor.warehouse.domain.user;

import hacklabor.warehouse.domain.items.Item;
import lombok.Value;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Value
public class Inventory {

    User user;
    Set<Item> items = new HashSet<>();
    Map<Long, Long> itemCount = new HashMap<>();

    private Inventory(User user) {
        this.user = user;
    }

    public static Inventory forUser(User user) {
        Inventory inventory = new Inventory(user);

        for (UserInventoryEvent event : user.getInventoryEvents()) {
            inventory = event.applyTo(inventory);
        }

        return inventory;
    }

    public void addItem(Item item, Long amount) {
        items.add(item);

        Long newItemCount = itemCount.containsKey(item.getId()) ?
                itemCount.get(item.getId()) + amount
                : amount;

        itemCount.put(item.getId(), newItemCount);
    }

    public void borrowItem(Item item, Long amount) {
        addItem(item, -1 * amount);
    }

    public Long getItemCount(Item item) {
        if (!itemCount.containsKey(item.getId())) {
            return 0L;
        }

        return itemCount.get(item.getId());
    }
}
