package hacklabor.warehouse.domain.user;

import hacklabor.warehouse.api.user.BorrowItemResponse;
import hacklabor.warehouse.api.user.UserInventoryEventResponse;
import hacklabor.warehouse.domain.items.Item;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.PositiveOrZero;

@Data
@Entity
@NoArgsConstructor
@Table(name = "borrow_item_events")
@EqualsAndHashCode(callSuper = true)
public class BorrowItem extends UserInventoryEvent {

    @ManyToOne
    @JoinColumn(name = "item_id", nullable = false)
    Item item;

    @PositiveOrZero
    Long itemCount;

    public BorrowItem(User user, Item item, Long itemCount) {
        super(user);
        this.item = item;
        this.itemCount = itemCount;
    }

    @Override
    public Inventory applyTo(Inventory inventory) {
        inventory.borrowItem(item, itemCount);
        return inventory;
    }

    @Override
    public UserInventoryEventResponse toResponse() {
        return BorrowItemResponse.of(this);
    }
}
