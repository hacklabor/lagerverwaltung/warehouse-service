package hacklabor.warehouse.domain.storage;

import org.springframework.data.repository.CrudRepository;

public interface StorageUnitRepository extends CrudRepository<StorageUnit, Long> {
}
