package hacklabor.warehouse.domain.storage;

import hacklabor.warehouse.api.storage.unit.CreateStorageUnitRequest;
import hacklabor.warehouse.api.storage.unit.UpdateStorageUnitRequest;
import hacklabor.warehouse.domain.items.Item;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "storage_units")
@Data
@NoArgsConstructor
public class StorageUnit {

    @Id
    @GenericGenerator(name = "UseExistingIdOtherwiseGenerateUsingIdentity", strategy = "hacklabor.warehouse.UseExistingIdOtherwiseGenerateUsingIdentity")
    @GeneratedValue(generator = "UseExistingIdOtherwiseGenerateUsingIdentity")
    @Column(unique = true, nullable = false)
    private Long id;

    private Long shelfId;

    @Column(name = "storage_row")
    private Long row;

    @Column(name = "storage_column")
    private Long column;

    @MapKeyColumn(name = "id")
    @ElementCollection
    @Getter(AccessLevel.PRIVATE)
    @Setter(AccessLevel.PRIVATE)
    private Map<Long, Long> itemCount = new HashMap<>();

    @ManyToMany
    @JoinTable(
            name = "storage_unit_items",
            joinColumns = @JoinColumn(name = "item_id"),
            inverseJoinColumns = @JoinColumn(name = "storage_unit_id"))
    @Setter(AccessLevel.PRIVATE)
    private Set<Item> items = new HashSet<>();

    public Set<Item> getItems() {
        return Collections.unmodifiableSet(this.items);
    }

    public static StorageUnit of(CreateStorageUnitRequest request) {
        StorageUnit storageUnit = new StorageUnit();

        storageUnit.setId(request.getId());
        storageUnit.setShelfId(request.getShelfId());
        storageUnit.setRow(request.getRow());
        storageUnit.setColumn(request.getColumn());

        return storageUnit;
    }

    public void addItem(Item item) {
        this.items.add(item);
        item.addToStorageUnit(this);
    }

    public void setItemCount(Item item, Long count) {
        if (!this.itemCount.containsKey(item.getId())) {
            this.itemCount.put(item.getId(), 0L);
        }
        this.itemCount.put(item.getId(), count);
    }

    public Long getItemCount(Item item) {
        if (!this.itemCount.containsKey(item.getId())) {
            return 0L;
        }

        return this.itemCount.get(item.getId());
    }

    public void increaseItemCount(Item item, Long amount) {
        Long newItemCount = itemCount.containsKey(item.getId()) ?
                itemCount.get(item.getId()) + amount
                : amount;

        itemCount.put(item.getId(), newItemCount);
    }

    public void decreaseItemCount(Item item, Long amount) {
        increaseItemCount(item, -1 * amount);
    }

    public StorageUnit update(UpdateStorageUnitRequest request, List<Item> items) {
        this.shelfId = request.getShelfId();
        this.row = request.getRow();
        this.column = request.getColumn();
        this.items.clear();
        for (Item item : items) {
            item.addToStorageUnit(this);
            this.items.add(item);
        }

        return this;
    }
}
