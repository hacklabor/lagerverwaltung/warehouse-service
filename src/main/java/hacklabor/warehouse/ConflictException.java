package hacklabor.warehouse;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Value;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Value
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@ResponseStatus(code = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {
    private String message;
}
