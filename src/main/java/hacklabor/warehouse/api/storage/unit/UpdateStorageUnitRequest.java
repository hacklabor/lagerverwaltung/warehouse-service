package hacklabor.warehouse.api.storage.unit;

import lombok.Data;

import java.util.List;

@Data
public class UpdateStorageUnitRequest {
    Long shelfId;
    Long row;
    Long column;
    List<Long> itemIds;
}
