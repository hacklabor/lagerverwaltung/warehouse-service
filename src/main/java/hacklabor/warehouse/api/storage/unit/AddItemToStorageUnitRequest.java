package hacklabor.warehouse.api.storage.unit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class AddItemToStorageUnitRequest {
    private Long itemId;
}
