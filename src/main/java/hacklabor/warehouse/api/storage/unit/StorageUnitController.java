package hacklabor.warehouse.api.storage.unit;

import hacklabor.warehouse.ConflictException;
import hacklabor.warehouse.NotFoundException;
import hacklabor.warehouse.api.Response;
import hacklabor.warehouse.api.items.UpdateItemCountRequest;
import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemRepository;
import hacklabor.warehouse.domain.storage.StorageUnit;
import hacklabor.warehouse.domain.storage.StorageUnitRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class StorageUnitController {

    private final StorageUnitRepository storageUnitRepository;
    private final ItemRepository itemRepository;

    public StorageUnitController(StorageUnitRepository storageUnitRepository, ItemRepository itemRepository) {
        this.storageUnitRepository = storageUnitRepository;
        this.itemRepository = itemRepository;
    }

    @GetMapping("/storageUnits")
    public Response<List<StorageUnitResponse>> getStorageUnits() {
        List<StorageUnitResponse> storageUnitResponses = ((List<StorageUnit>) storageUnitRepository.findAll())
                .stream()
                .map(StorageUnitResponse::of)
                .collect(Collectors.toList());
        return Response.ok(storageUnitResponses);
    }

    @GetMapping("/storageUnits/{id}")
    public Response<StorageUnitResponse> findById(@PathVariable Long id) {
        StorageUnit storageUnit = storageUnitRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id " + id));

        return Response.ok(StorageUnitResponse.of(storageUnit));
    }

    @PostMapping("/storageUnits")
    @ResponseStatus(HttpStatus.CREATED)
    public Response createStorageUnit(@RequestBody(required = false) CreateStorageUnitRequest request) {

        if (request.getId() != null && storageUnitRepository.existsById(request.getId())) {
            throw new ConflictException(String.format("Storage unit with id %s already exists.", request.getId()));
        }

        StorageUnit storageUnit = storageUnitRepository.save(StorageUnit.of(request));

        return Response.ok(StorageUnitResponse.of(storageUnit));
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/storageUnits/{storageUnitId}/items")
    public Response<StorageUnitResponse> addItemToStorageUnit(@PathVariable Long storageUnitId, @RequestBody AddItemToStorageUnitRequest request) {
        StorageUnit storageUnit = storageUnitRepository.findById(storageUnitId)
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id: " + storageUnitId));
        Item item = itemRepository.findById(request.getItemId())
                .orElseThrow(() -> new NotFoundException("Could not find item with id: " + request.getItemId()));

        storageUnit.addItem(item);
        storageUnit = storageUnitRepository.save(storageUnit);

        return Response.ok(StorageUnitResponse.of(storageUnit));
    }

    @PutMapping("/storageUnits/{storageUnitId}")
    public Response<StorageUnitResponse> updateStorageUnit(@PathVariable Long storageUnitId, @RequestBody UpdateStorageUnitRequest request) {
        StorageUnit storageUnit = storageUnitRepository.findById(storageUnitId)
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id: " + storageUnitId));
        List<Item> items = (List<Item>) itemRepository.findAllById(request.getItemIds());

        storageUnit = storageUnit.update(request, items);
        storageUnit = storageUnitRepository.save(storageUnit);

        return Response.ok(StorageUnitResponse.of(storageUnit));
    }

    @PostMapping("/storageUnits/{storageUnitId}/items/{itemId}")
    public Response<StorageUnitResponse> updateItemCount(@PathVariable Long storageUnitId, @PathVariable Long itemId, @RequestBody UpdateItemCountRequest request) {
        StorageUnit storageUnit = storageUnitRepository.findById(storageUnitId)
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id: " + storageUnitId));
        Item item = itemRepository.findById(itemId)
                .orElseThrow(() -> new NotFoundException("Could not find item with id: " + itemId));

        storageUnit.setItemCount(item, request.getCount());
        storageUnit = storageUnitRepository.save(storageUnit);

        return Response.ok(StorageUnitResponse.of(storageUnit));
    }
}
