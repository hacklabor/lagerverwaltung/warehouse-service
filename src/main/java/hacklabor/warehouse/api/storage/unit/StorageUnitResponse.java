package hacklabor.warehouse.api.storage.unit;

import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemTypes;
import hacklabor.warehouse.domain.items.ReplaceableItem;
import hacklabor.warehouse.domain.storage.StorageUnit;
import lombok.Data;
import lombok.Value;

import java.util.List;
import java.util.stream.Collectors;

@Value
class StorageUnitResponse {
    Long id;
    Long shelfId;
    Long row;
    Long column;
    List<StorageUnitItemResponse> items;

    static StorageUnitResponse of(StorageUnit storageUnit) {
        List<StorageUnitItemResponse> itemResponses = storageUnit.getItems().stream()
                .map(item -> StorageUnitItemResponse.of(item, storageUnit.getItemCount(item)))
                .collect(Collectors.toList());

        return new StorageUnitResponse(
                storageUnit.getId(),
                storageUnit.getShelfId(),
                storageUnit.getRow(),
                storageUnit.getColumn(),
                itemResponses
        );
    }

    @Data
    private static class StorageUnitItemResponse {

        Long id;
        ItemTypes itemType;
        String name;
        String description;
        String replacementUrl;
        String imageUrl;
        Long availableCount;

        static StorageUnitItemResponse of(Item item, Long availableCount) {
            StorageUnitItemResponse response = new StorageUnitItemResponse();

            response.setId(item.getId());
            response.setName(item.getName());
            response.setDescription(item.getDescription());
            response.setImageUrl(item.getImageUrl());

            if (item instanceof ReplaceableItem) {
                response.setItemType(ItemTypes.REPLACEABLE);
                response.setReplacementUrl(((ReplaceableItem) item).getReplacementUrl());
            }
            response.setAvailableCount(availableCount);

            return response;
        }
    }
}
