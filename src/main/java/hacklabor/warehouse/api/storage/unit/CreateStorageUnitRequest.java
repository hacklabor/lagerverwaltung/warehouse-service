package hacklabor.warehouse.api.storage.unit;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class CreateStorageUnitRequest {

    Long id;
    Long shelfId;
    Long row;
    Long column;

}
