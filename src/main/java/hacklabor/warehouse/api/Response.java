package hacklabor.warehouse.api;

import lombok.AllArgsConstructor;
import lombok.Value;

@Value
@AllArgsConstructor
public class Response<T> {
    private T data;
    private String errorMessage;

    public static <T> Response<T> ok(T data) {
        return new Response<>(data, null);
    }

    public static Response error(String errorMessage) {
        return new Response(null, errorMessage);
    }

    public static Response error(Iterable<String> errorMessages) {
        return new Response(null, String.join("\n", errorMessages));
    }
}
