package hacklabor.warehouse.api.items;

import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemTypes;
import hacklabor.warehouse.domain.items.ReplaceableItem;
import hacklabor.warehouse.domain.storage.StorageUnit;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class ItemResponse {

    Long id;
    ItemTypes itemType;
    String name;
    String description;
    String replacementUrl;
    String imageUrl;
    boolean isDeleted;
    List<ItemStorageUnitResponse> storageUnits;

    public static ItemResponse of(Item item) {
        ItemResponse response = new ItemResponse();
        response.setId(item.getId());
        response.setName(item.getName());
        response.setDescription(item.getDescription());
        response.setImageUrl(item.getImageUrl());
        response.setDeleted(item.isDeleted());

        List<ItemStorageUnitResponse> storageUnitResponses = item.getStorageUnits().stream()
                .map((StorageUnit storageUnit) -> ItemStorageUnitResponse.of(storageUnit, item))
                .collect(Collectors.toList());
        response.setStorageUnits(storageUnitResponses);

        if (item instanceof ReplaceableItem) {
            response.setItemType(ItemTypes.REPLACEABLE);
            response.setReplacementUrl(((ReplaceableItem) item).getReplacementUrl());
        }

        return response;
    }

    @Data
    private static class ItemStorageUnitResponse {
        Long id;
        Long shelfId;
        Long row;
        Long column;
        Long availableCount;

        private static ItemStorageUnitResponse of(StorageUnit storageUnit, Item item) {
            ItemStorageUnitResponse response = new ItemStorageUnitResponse();

            response.setId(storageUnit.getId());
            response.setShelfId(storageUnit.getShelfId());
            response.setRow(storageUnit.getRow());
            response.setColumn(storageUnit.getColumn());
            response.setAvailableCount(storageUnit.getItemCount(item));

            return response;
        }
    }
}
