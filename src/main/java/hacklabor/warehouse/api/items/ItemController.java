package hacklabor.warehouse.api.items;

import hacklabor.warehouse.NotFoundException;
import hacklabor.warehouse.api.Response;
import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

@RestController
public class ItemController {

    private final ItemRepository itemRepository;

    public ItemController(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @PostMapping("/items")
    @ResponseStatus(HttpStatus.CREATED)
    public Response<ItemResponse> create(@RequestBody CreateItemRequest request) {
        Item item = Item.create(request);

        item = itemRepository.save(item);

        return Response.ok(ItemResponse.of(item));
    }

    @GetMapping("/items/{id}")
    public Response<ItemResponse> findById(@PathVariable Long id) {
        Item item = itemRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find item with id " + id));

        return Response.ok(ItemResponse.of(item));
    }

    @GetMapping("/items")
    public Response<List<ItemResponse>> getItems(
            @RequestParam(required = false) String name,
            @RequestParam(required = false, defaultValue = "true") boolean includeDeleted) {

        Iterable<Item> items = name != null && !name.isEmpty() ?
                itemRepository.findByNameContainingIgnoreCase(name)
                : itemRepository.findAll();

        Stream<Item> results = StreamSupport.stream(items.spliterator(), false);

        if (!includeDeleted) {
            results = results.filter(item -> !item.isDeleted());
        }

        List<ItemResponse> itemResponses = results
                .map(ItemResponse::of)
                .collect(Collectors.toList());

        return Response.ok(itemResponses);
    }

    @PutMapping("/items/{id}")
    public Response<ItemResponse> updateItem(@PathVariable Long id, @RequestBody UpdateItemRequest request) {
        Item item = itemRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find item with id: " + id));

        item = item.update(request);
        item = itemRepository.save(item);

        return Response.ok(ItemResponse.of(item));
    }

    @DeleteMapping("/items/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteItem(@PathVariable Long id) {
        Item item = itemRepository.findById(id)
                .orElseThrow(() -> new NotFoundException("Could not find item with id " + id));

        item.setDeleted(true);
        itemRepository.save(item);
    }
}
