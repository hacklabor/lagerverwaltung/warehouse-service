package hacklabor.warehouse.api.items;

import hacklabor.warehouse.domain.items.ItemTypes;
import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CreateItemRequest {

    @NonNull
    private ItemTypes itemType;

    private Long id;
    private String name;
    private String description;
    private String replacementUrl;
    private String imageUrl;

    public CreateItemRequest(ItemTypes itemType) {
        this.itemType = itemType;
    }
}
