package hacklabor.warehouse.api.items;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UpdateItemRequest {
    private String name;
    private String description;
    private String replacementUrl;
    private String imageUrl;
}
