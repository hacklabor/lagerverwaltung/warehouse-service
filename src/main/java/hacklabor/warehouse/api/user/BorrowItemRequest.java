package hacklabor.warehouse.api.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BorrowItemRequest {
    Long storageUnitId;
    Long itemId;
    Long itemCount;
}
