package hacklabor.warehouse.api.user;

import lombok.Data;

@Data
public class CreateUserRequest {
    Long id;
    String userName;
    String rfidChipId;
}
