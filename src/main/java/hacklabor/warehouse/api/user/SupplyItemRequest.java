package hacklabor.warehouse.api.user;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SupplyItemRequest {
    Long storageUnitId;
    Long itemId;
    Long itemCount;
}
