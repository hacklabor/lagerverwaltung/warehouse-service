package hacklabor.warehouse.api.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.ZonedDateTime;

@Data
@NoArgsConstructor
public class UserInventoryEventResponse {
    private Long id;
    private Long userId;
    private ZonedDateTime createdAt;
    private ZonedDateTime effectiveAt;

    protected UserInventoryEventResponse(Long id, Long userId, ZonedDateTime createdAt, ZonedDateTime effectiveAt) {
        this.id = id;
        this.userId = userId;
        this.createdAt = createdAt;
        this.effectiveAt = effectiveAt;
    }
}
