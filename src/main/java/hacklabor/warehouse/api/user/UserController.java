package hacklabor.warehouse.api.user;

import hacklabor.warehouse.NotFoundException;
import hacklabor.warehouse.api.Response;
import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemRepository;
import hacklabor.warehouse.domain.storage.StorageUnit;
import hacklabor.warehouse.domain.storage.StorageUnitRepository;
import hacklabor.warehouse.domain.user.*;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("/users")
public class UserController {

    private final UserRepository userRepository;
    private final UserInventoryEventRepository userInventoryEventRepository;
    private final StorageUnitRepository storageUnitRepository;
    private final ItemRepository itemRepository;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Response<UserResponse> createUser(@RequestBody CreateUserRequest request) {
        User user = User.from(request);

        user = userRepository.save(user);

        return Response.ok(UserResponse.of(user));
    }

    @GetMapping("/")
    public Response<List<UserResponse>> getUsers(){
        List<UserResponse> userResponses = ((List<User>)userRepository.findAll())
                .stream()
                .map(UserResponse::of)
                .collect(Collectors.toList());

        return Response.ok(userResponses);
    }

    @GetMapping("/{userId}")
    public Response<UserResponse> getUserById(@PathVariable Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Could not find user with id " + userId));

        return Response.ok(UserResponse.of(user));
    }

    @GetMapping("/rfidChipId/{rfidChipId}")
    public Response<UserResponse> getUserByRfidChipId(@PathVariable String rfidChipId) {
        User user = userRepository.findByRfidChipId(rfidChipId)
                .orElseThrow(() -> new NotFoundException("Could not find user with id " + rfidChipId));

        return Response.ok(UserResponse.of(user));
    }

    @PostMapping("/{userId}/supply")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void supplyItem(@PathVariable Long userId, @RequestBody SupplyItemRequest request){
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Could not find user with id " + userId));
        StorageUnit storageUnit = storageUnitRepository.findById(request.getStorageUnitId())
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id " + request.getStorageUnitId()));
        Item item = itemRepository.findById(request.getItemId())
                .orElseThrow(() -> new NotFoundException("Could not find item with id " + request.getItemId()));

        storageUnit.addItem(item);
        storageUnit.increaseItemCount(item, request.getItemCount());
        storageUnitRepository.save(storageUnit);

        userInventoryEventRepository.save(new SupplyItem(user, item, request.getItemCount()));
    }

    @PostMapping("/{userId}/borrow")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrowItem(@PathVariable Long userId, @RequestBody SupplyItemRequest request){
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Could not find user with id " + userId));
        StorageUnit storageUnit = storageUnitRepository.findById(request.getStorageUnitId())
                .orElseThrow(() -> new NotFoundException("Could not find storage unit with id " + request.getStorageUnitId()));
        Item item = itemRepository.findById(request.getItemId())
                .orElseThrow(() -> new NotFoundException("Could not find item with id " + request.getItemId()));

        storageUnit.addItem(item);
        storageUnit.decreaseItemCount(item, request.getItemCount());
        storageUnitRepository.save(storageUnit);

        userInventoryEventRepository.save(new BorrowItem(user, item, request.getItemCount()));
    }

    @GetMapping("/{userId}/inventory/history")
    public Response<List<UserInventoryEventResponse>> getInventoryHistory(@PathVariable Long userId) {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new NotFoundException("Could not find user with id " + userId));

        List<UserInventoryEventResponse> eventResponses = user.getInventoryEvents().stream()
                .map(UserInventoryEvent::toResponse)
                .collect(Collectors.toList());

        return Response.ok(eventResponses);
    }
}
