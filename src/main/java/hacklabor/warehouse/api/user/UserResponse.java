package hacklabor.warehouse.api.user;

import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.user.Inventory;
import hacklabor.warehouse.domain.user.User;
import lombok.Data;
import lombok.Value;

import java.util.HashMap;
import java.util.Map;

@Data
public class UserResponse {
    private Long id;
    private String userName;
    private String rfidChipId;
    private Map<Long, ItemCountResponse> inventory = new HashMap<>();

    public static UserResponse of(User user) {
        UserResponse userResponse = new UserResponse();

        userResponse.setId(user.getId());
        userResponse.setUserName(user.getUserName());
        userResponse.setRfidChipId(user.getRfidChipId());

        Inventory inventory = Inventory.forUser(user);
        for (Item item : inventory.getItems()) {
            userResponse.inventory.put(item.getId(), new ItemCountResponse(inventory.getItemCount(item)));
        }

        return userResponse;
    }

    @Value
    private static class ItemCountResponse {
        Long count;
    }
}
