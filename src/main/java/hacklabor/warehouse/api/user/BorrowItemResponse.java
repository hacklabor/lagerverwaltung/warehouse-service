package hacklabor.warehouse.api.user;

import hacklabor.warehouse.domain.user.BorrowItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class BorrowItemResponse extends UserInventoryEventResponse {

    final String type = "BORROW";
    Long itemId;
    Long itemCount;

    public BorrowItemResponse(BorrowItem event) {
        super(event.getId(), event.getUser().getId(), event.getCreatedAt(), event.getEffectiveAt());
    }

    public static BorrowItemResponse of(BorrowItem event) {
        BorrowItemResponse response = new BorrowItemResponse(event);
        response.setItemId(event.getItem().getId());
        response.setItemCount(event.getItemCount());
        return response;
    }
}
