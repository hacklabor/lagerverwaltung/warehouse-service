package hacklabor.warehouse.api.user;

import hacklabor.warehouse.domain.user.SupplyItem;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
public class SupplyItemResponse extends UserInventoryEventResponse {

    final String type = "SUPPLY";
    Long itemId;
    Long itemCount;

    public SupplyItemResponse(SupplyItem event) {
        super(event.getId(), event.getUser().getId(), event.getCreatedAt(), event.getEffectiveAt());
    }

    public static SupplyItemResponse of(SupplyItem event) {
        SupplyItemResponse response = new SupplyItemResponse(event);
        response.setItemId(event.getItem().getId());
        response.setItemCount(event.getItemCount());
        return response;
    }
}

