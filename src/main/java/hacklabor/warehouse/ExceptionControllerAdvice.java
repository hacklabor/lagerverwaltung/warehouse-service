package hacklabor.warehouse;

import hacklabor.warehouse.api.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public final class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NotFoundException.class)
    private ResponseEntity<Response> handleNotFound(NotFoundException ex, HttpServletRequest request) {
        return new ResponseEntity<>(Response.error(ex.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConflictException.class)
    private ResponseEntity<Response> handleConflict(NotFoundException ex, HttpServletRequest request) {
        return new ResponseEntity<>(Response.error(ex.getMessage()), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    private ResponseEntity<Response> handleConstraintViolationException(ConstraintViolationException ex, HttpServletRequest request) {
        List<String> errorMessages = getErrorMessages(ex);

        return new ResponseEntity<>(Response.error(errorMessages), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(Exception.class)
    private ResponseEntity<Response> handleException(Exception ex, HttpServletRequest request) {
        if (isCausedBy(ex, ConstraintViolationException.class)) {
            ConstraintViolationException violationException = getCauseByType(ex, ConstraintViolationException.class);
            return handleConstraintViolationException(violationException, request);
        }

        return new ResponseEntity<>(Response.error(ex.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private <T> boolean isCausedBy(Exception exception, Class<? extends Throwable> causeType) {
        Throwable cause = exception.getCause();

        while (cause != null) {
            if (causeType.isInstance(cause)) {
                return true;
            }
            cause = cause.getCause();
        }

        return false;
    }

    private <T> T getCauseByType(Exception exception, Class<? extends Throwable> causeType) {
        Throwable cause = exception.getCause();

        while (cause != null) {
            if (causeType.isInstance(cause)) {
                return (T) cause;
            }
            cause = cause.getCause();
        }

        throw new IllegalArgumentException("Could not extract cause of type " + causeType);
    }

    private static List<String> getErrorMessages(ConstraintViolationException violationException) {
        return violationException.getConstraintViolations()
                .stream()
                .map(violation -> String.format("%s value '%s' %s", violation.getPropertyPath(),
                        violation.getInvalidValue(), violation.getMessage()))
                .collect(Collectors.toList());
    }
}
