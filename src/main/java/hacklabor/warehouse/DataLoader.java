package hacklabor.warehouse;

import hacklabor.warehouse.api.items.CreateItemRequest;
import hacklabor.warehouse.domain.items.Item;
import hacklabor.warehouse.domain.items.ItemRepository;
import hacklabor.warehouse.domain.items.ItemTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class DataLoader implements ApplicationRunner {

    private final ItemRepository itemRepository;

    @Autowired
    public DataLoader(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public void run(ApplicationArguments args) {
        //add default date here
    }

    private void saveTestItem(String title) {
        CreateItemRequest createItemRequest = CreateItemRequest.builder()
                .itemType(ItemTypes.REPLACEABLE)
                .name(title)
                .description("Lorem ipsum")
                .replacementUrl("http://example.com")
                .imageUrl("http://example.com")
                .build();
        itemRepository.save(Item.create(createItemRequest));
    }
}
