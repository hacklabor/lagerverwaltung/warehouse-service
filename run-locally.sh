#!/usr/bin/env sh

./start-dependencies.sh
JDBC_URL=jdbc:postgresql://localhost:5432/postgres DB_USERNAME=postgres DB_PASSWORD=mysecretpassword ./gradlew bootRun
