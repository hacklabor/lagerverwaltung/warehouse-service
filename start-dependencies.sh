#!/usr/bin/env sh
docker run --rm -d --name hal-warehouse-db -e POSTGRES_PASSWORD=mysecretpassword -p 5432:5432 postgres
